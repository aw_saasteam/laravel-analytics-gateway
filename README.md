## After installation
```bash
php artisan vendor:publish --provider="Aheadworks\Analytics\Gateway\ServiceProvider"
```

### For Discord notifications:


1. composer require marvinlabs/laravel-discord-logger:v1.3.2
2. Create a Webhook on Discord application
3. Paste in `config/logging.php` the next lines:
```bash
'discord' => [
    'driver' => 'custom',
    'via'    => MarvinLabs\DiscordLogger\Logger::class,
    'level'  => 'error',
    'url'    => env('LOG_DISCORD_WEBHOOK_URL', ''),
    'ignore_exceptions' => env('LOG_DISCORD_IGNORE_EXCEPTIONS', false),
],
```
4. Change options in `config/analytics.php` file:
```bash
'errors' => [
    'logging' => true,
    'log_channel' => 'discord'
],
```
