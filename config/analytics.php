<?php

use Aheadworks\Analytics\Gateway\Channels\CustomerIo;
use Aheadworks\Analytics\Gateway\Channels\Debug;
use Aheadworks\Analytics\Gateway\Channels\Mixpanel;
use Aheadworks\Analytics\Gateway\Channels\Rudderstack;
use Aheadworks\Analytics\Gateway\Channels\SegmentIo;
use Aheadworks\Analytics\Gateway\Channels\Stack;
use Aheadworks\Analytics\Gateway\Contracts\Channels\Channel;

return [
    'default' => [

        /*
        |--------------------------------------------------------------------------
        | Default Channel
        |--------------------------------------------------------------------------
        |
        | This option defines the default analytics channel.
        | The name specified in this option should match
        | one of the channels defined in the "channels" configuration array.
        |
        */
        'channel' => env('ANALYTICS_CHANNEL', Channel::DEBUG),
    ],

    /*
    |--------------------------------------------------------------------------
    | Analytics Channels
    |--------------------------------------------------------------------------
    |
    | Here you may configure the analytics channels for your application.
    |
    | Available Channels: "Stack", "Rudderstack", "Segment", "Debug", "Mixpanel"
    |
    | "Stack" channel combines other channels listed in the 'channels' attribute
    |
    */
    'channels' => [
        Channel::STACK => [
            'driver'   => Stack::class,
            'channels' => [Channel::DEBUG],
        ],
        Channel::DEBUG => [
            'driver' => Debug::class,
        ],
        Channel::RUDDERSTACK => [
            'driver' => Rudderstack::class,
            'write_key' => env('RUDDERSTACK_WRITE_KEY', ''),
            'data_plane_url' => env('RUDDERSTACK_DATA_PLAN_URL', '')
        ],
        Channel::SEGMENT_IO => [
            'driver' => SegmentIo::class,
            'write_key' => env('SEGMENT_WRITE_KEY', ''),
        ],
        Channel::MIXPANEL => [
            'driver' => Mixpanel::class,
            'token' => env('MIXPANEL_PROJECT_TOKEN', ''),
            'options' => [] //@see http://mixpanel.github.io/mixpanel-php/classes/Mixpanel.html
        ],
        Channel::CUSTOMER_IO => [
            'driver' => CustomerIo::class,
            'site_id' => env('CUSTOMER_IO_SITE_ID', ''),
            'api_key' => env('CUSTOMER_IO_API_KEY', '')
        ]
    ],

    /*
    |--------------------------------------------------------------------------
    | Errors handling
    |--------------------------------------------------------------------------
    |
    | Here you can configure the behavior when an error occurs.
    |
    */
    'errors' => [
        'reporting' => true,

        'logging' => false,
        'log_channel' => null
    ],
];
