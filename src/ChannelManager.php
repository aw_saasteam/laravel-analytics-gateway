<?php
namespace Aheadworks\Analytics\Gateway;

use Aheadworks\Analytics\Gateway\Contracts\Channels\Channel as ChannelContract;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\Arr;

class ChannelManager
{
    /**
     * @var Application
     */
    protected $app;

    /**
     * @var ChannelContract[]
     */
    protected $channels = [];

    /**
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * @param string|null $name
     * @return ChannelContract
     */
    public function channel(string $name = null): ChannelContract
    {
        $name = $name ?: $this->getDefaultChannelName();

        return $this->get($name);
    }

    /**
     * @return ChannelContract[]
     */
    public function getActivatedChannels(): array
    {
        return $this->channels;
    }

    /**
     * @param string $name
     * @return ChannelContract
     */
    protected function get(string $name): ChannelContract
    {
        if (!array_key_exists($name, $this->channels)) {
            $this->channels[$name] = $this->create($name);
        }

        return $this->channels[$name];
    }

    /**
     * @param string $name
     *
     * @return ChannelContract
     * @throws BindingResolutionException
     */
    protected function create(string $name): ChannelContract
    {
        $config = config("analytics.channels.{$name}") ?? [];

        if (empty($config['driver'])) {
            throw new \InvalidArgumentException("Channel [{$name}] does not have a configured driver.");
        }

        $channel = $this->app->make(
            $config['driver'],
            Arr::except($config, ['driver'])
        );

        if (!$channel instanceof ChannelContract) {
            throw new \InvalidArgumentException("Channel [{$name}] does not implement contract.");
        }

        return $channel;
    }

    /**
     * @return string
     */
    public function getDefaultChannelName(): string
    {
        $name = config('analytics.default.channel');
        if (empty($name)) {
            $name = ChannelContract::DEBUG;
        }
        return $name;
    }

    /**
     * Dynamically call the default channel instance.
     *
     * @param string $method
     * @param array  $parameters
     * @return mixed
     * @throws BindingResolutionException
     */
    public function __call(string $method, array $parameters)
    {
        return $this->channel()->$method(...$parameters);
    }
}
