<?php
namespace Aheadworks\Analytics\Gateway\Channels;

use Aheadworks\Analytics\Gateway\Contracts\Channels\Channel;
use Aheadworks\Analytics\Gateway\Contracts\Channels\PayloadTransformer;
use Illuminate\Contracts\Container\BindingResolutionException;

abstract class AbstractChannel implements Channel
{
    /**
     * @var PayloadTransformer[]
     */
    private $transformers = [];

    /**
     * @param array|string ...$transformers
     * @return void
     * @throws BindingResolutionException
     */
    protected function setTransformers(...$transformers): void
    {
        foreach ($transformers as $transformer) {
            if (is_array($transformer)) {
                $this->transformers = array_merge($this->transformers, $transformer);
            } elseif (is_string($transformer)) {
                $this->transformers[] = $transformer;
            }
        }

        $this->makeTransformersObjects();

    }

    /**
     * @param array $payload
     * @return array
     */
    protected function transform(array $payload): array
    {
        foreach ($this->transformers as $transformer) {
            $payload = $transformer->transform($payload);
        }

        return $payload;
    }

    /**
     * @return void
     * @throws BindingResolutionException
     */
    private function makeTransformersObjects(): void
    {
        $this->transformers = array_reduce(
            $this->transformers,
            function(array $carry, $transformer) {
                if (is_string($transformer)) {
                    $transformer = app()->make($transformer);
                }
                $carry[] = $transformer;

                return $carry;
            },
            []
        );
    }
}
