<?php
namespace Aheadworks\Analytics\Gateway\Channels\Clients;

use Aheadworks\Analytics\Gateway\Exceptions\CustomerIo\IdentifiersNotValidException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Http\Message\ResponseInterface;

class CustomerIo extends Client
{
    private const HOST = 'https://track.customer.io/api/';

    /**
     * @param string $site_id
     * @param string $api_key
     */
    public function __construct(string $site_id, string $api_key)
    {
        parent::__construct([
            'headers' => [
                'Authorization' => "Basic ". base64_encode("$site_id:$api_key"),
                'content-type' => 'application/json'
            ]
        ]);
    }

    /**
     * Identify
     *
     * @param array $identifiers
     * @param array $data
     *
     * @return ResponseInterface
     *
     * @throws GuzzleException
     * @throws IdentifiersNotValidException
     */
    public function identify(array $identifiers, array $data)
    {
        $this->checkIdentifiers($identifiers);
        $body = array_merge([
            'type' => 'person',
            'action' => 'identify',
            'identifiers' => $identifiers
        ], $data);

        return $this->request(
            'POST',
            self::HOST.'v2/entity',
            [
                'body' => json_encode($body)
            ]
        );
    }

    /**
     * Track
     *
     * @param array $identifiers
     * @param array $data
     *
     * @return ResponseInterface
     *
     * @throws GuzzleException
     * @throws IdentifiersNotValidException
     */
    public function track(array $identifiers, array $data)
    {
        $this->checkIdentifiers($identifiers);
        $body = array_merge([
            'type' => 'person',
            'action' => 'event',
            'identifiers' => $identifiers
        ], $data);

        return $this->request(
            'POST',
            self::HOST.'v2/entity',
            [
                'body' => json_encode($body)
            ]
        );
    }

    /**
     * @return void
     */
    public function flush(): void
    {
    }

    /**
     * Check identifiers
     *
     * @param array $identifiers
     * @return void
     *
     * @throws IdentifiersNotValidException
     */
    private function checkIdentifiers(array $identifiers): void
    {
        if (count($identifiers) !== 1) {
            $exceptionMessage = empty($identifiers)
                ? 'User identifier is not provided.'
                : 'Identifier can not be multiple. Please choose one of either id, email, or cio_id';
            throw new IdentifiersNotValidException($exceptionMessage);
        }

        $identifierName = key($identifiers);
        if (!in_array($identifierName, ['id', 'email', 'cio_id'])) {
            throw new IdentifiersNotValidException(
                "Invalid identifier key $identifierName. Please choose one of either id, email, or cio_id "
            );
        }
    }
}
