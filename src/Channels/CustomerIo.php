<?php
namespace Aheadworks\Analytics\Gateway\Channels;

use Aheadworks\Analytics\Gateway\Channels\Clients\CustomerIo as CustomerIoClient;
use Aheadworks\Analytics\Gateway\Channels\Transformers\CarbonToUnixTimestampTransformer;
use Aheadworks\Analytics\Gateway\Channels\Transformers\CustomerIoReservedPropertiesTransformer;
use Exception;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Carbon;
use RuntimeException;

class CustomerIo extends AbstractChannel
{
    /**
     * @var CustomerIoClient
     */
    private $client;

    /**
     * @param string $site_id
     * @param string $api_key
     * @param array $transformers
     */
    public function __construct(string $site_id, string $api_key, array $transformers = [])
    {
        if (empty($site_id)) {
            throw new RuntimeException("Customer.io: [site_id] must be specified");
        }
        if (empty($api_key)) {
            throw new RuntimeException("Customer.io: [api_key] must be specified");
        }

        $this->client = app()->make(CustomerIoClient::class, compact('site_id', 'api_key'));

        $this->setTransformers(
            CustomerIoReservedPropertiesTransformer::class,
            CarbonToUnixTimestampTransformer::class,
            $transformers
        );
    }

    /**
     * @param int|string $analyzableId
     * @param array      $properties
     * @return void
     */
    public function identify($analyzableId, array $properties): void
    {
        try {
            $properties = $this->transform($properties);

            $this->client->identify(
                ['id' => $analyzableId],
                ['attributes' => $properties]
            );
        } catch (Exception|GuzzleException $exception) {
            throw new RuntimeException(
                'CustomerIo: Could not identify user. Error code ' . $exception->getCode() .
                ' with message: ' . $exception->getMessage(), $exception->getCode(), $exception
            );
        }
    }

    /**
     * @param int|string $analyzableId
     * @param string      $name
     * @param array       $eventProperties
     * @param Carbon|null $time
     * @return void
     */
    public function track($analyzableId, string $name, array $eventProperties, ?Carbon $time = null): void
    {
        try {
            $eventProperties = $this->transform($eventProperties);

            $this->client->track(['id' => $analyzableId], [
                'name' => $name,
                'attributes' => $eventProperties,
                'timestamp' => $time ? $time->timestamp : 0
            ]);
        } catch (Exception|GuzzleException $exception) {
           throw new RuntimeException(
                'CustomerIo: Event could not be tracked. Error code ' . $exception->getCode() .
                ' with message: ' . $exception->getMessage(), $exception->getCode(), $exception
            );
        }
    }

    /**
     * @return void
     */
    public function flush(): void
    {
        $this->client->flush();
    }
}
