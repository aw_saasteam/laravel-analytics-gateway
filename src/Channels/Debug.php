<?php
namespace Aheadworks\Analytics\Gateway\Channels;

use Aheadworks\Analytics\Gateway\Contracts\Channels\Channel;
use Illuminate\Support\Carbon;
use Psr\Log\LoggerInterface;

class Debug implements Channel
{
    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param int|string $analyzableId
     * @param array      $properties
     * @return void
     */
    public function identify($analyzableId, array $properties): void
    {
        $this->logger->debug('Analytics::identify', compact('analyzableId', 'properties'));
    }

    /**
     * @param int|string $analyzableId
     * @param string $name
     * @param array  $eventProperties
     * @param Carbon $time
     * @return void
     */
    public function track($analyzableId, string $name, array $eventProperties, Carbon $time): void
    {
        $this->logger->debug('Analytics::track', compact('analyzableId', 'name', 'eventProperties', 'time'));
    }

    /**
     * @return void
     */
    public function flush(): void
    {
        $this->logger->debug('Analytics::flush');
    }
}
