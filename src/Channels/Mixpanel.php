<?php
namespace Aheadworks\Analytics\Gateway\Channels;

use Aheadworks\Analytics\Gateway\Channels\Transformers\CarbonToIsoStringTransformer;
use Aheadworks\Analytics\Gateway\Channels\Transformers\MixpanelReservedPropertiesTransformer;
use Exception;
use Illuminate\Support\Carbon;
use RuntimeException;

class Mixpanel extends AbstractChannel
{
    /**
     * @var \Mixpanel
     */
    private $mixpanelSdk;

    /**
     * @param string $token
     * @param array  $options
     * @param array  $transformers
     */
    public function __construct(string $token, array $options = [], array $transformers = [])
    {
        if (empty($token)) {
            throw new RuntimeException('Mixpanel: [token] must be specified');
        }

        $this->mixpanelSdk = \Mixpanel::getInstance($token, $options);

        $this->setTransformers(
            CarbonToIsoStringTransformer::class,
            MixpanelReservedPropertiesTransformer::class,
            $transformers
        );
    }

    /**
     * Set properties on a user record. If the profile does not exist, it creates it with these properties.
     * If it does exist, it sets the properties to these values, overwriting existing values.
     * Identify the user to associate to tracked events.
     *
     *
     * @param int|string $analyzableId
     * @param array      $properties
     * @return void
     */
    public function identify($analyzableId, array $properties): void
    {
        try {
            $properties = $this->transform($properties);

            $this->mixpanelSdk->people->set($analyzableId, $properties, '0', true);
            $this->mixpanelSdk->identify($analyzableId);
        } catch (Exception $exception) {
            throw new RuntimeException(
                'MixPanel: Could not identify user. Error code ' . $exception->getCode() .
                ' with message: ' . $exception->getMessage(), $exception->getCode(), $exception
            );
        }
    }

    /**
     * @param int|string $analyzableId
     * @param string      $name
     * @param array       $eventProperties
     * @param Carbon|null $time
     * @return void
     */
    public function track($analyzableId, string $name, array $eventProperties, ?Carbon $time = null): void
    {
        try {
            if ($time) {
                $eventProperties['time'] = $time;
            }

            $eventProperties = $this->transform($eventProperties);

            $this->mixpanelSdk->track(
                $name,
                array_merge($eventProperties, [
                    'distinct_id' => $analyzableId
                ])
            );
        } catch (Exception $exception) {
            throw new RuntimeException(
                'MixPanel: Event could not be tracked. Error code ' . $exception->getCode() .
                ' with message: ' . $exception->getMessage(), $exception->getCode(), $exception
            );
        }
    }

    /**
     * @return void
     */
    public function flush(): void
    {
        $this->mixpanelSdk->flush();
    }
}
