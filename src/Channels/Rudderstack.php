<?php
namespace Aheadworks\Analytics\Gateway\Channels;

use Aheadworks\Analytics\Gateway\Channels\Transformers\CarbonToIsoStringTransformer;
use Exception;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;
use Rudder;
use RuntimeException;

class Rudderstack extends AbstractChannel
{
    /**
     * @param string $write_key
     * @param string $data_plane_url
     * @param array  $transformers
     * @throws Exception
     */
    public function __construct(string $write_key, string $data_plane_url, array $transformers = []) {
        if (empty($write_key)) {
            throw new RuntimeException('Rudderstack: [write_key] must be specified');
        }

        Rudder::init(
            $write_key,
            [
                'data_plane_url' => $data_plane_url,
                'ssl' => true,
                'max_queue_size' => 100,
                'curl_timeout' => 60
            ]
        );

        $this->setTransformers(CarbonToIsoStringTransformer::class, $transformers);
    }

    /**
     * @param int|string $analyzableId
     * @param array      $properties
     * @return void
     */
    public function identify($analyzableId, array $properties): void
    {
        try {
            $properties = $this->transform($properties);

            Rudder::identify(array(
                'userId' => $analyzableId,
                'traits' => $this->transform($properties),
                'timestamp' => 0
            ));
        } catch (Exception $exception) {
            throw new RuntimeException(
                'RudderStack: Could not identify user. Error code ' . $exception->getCode() .
                ' with message: ' . $exception->getMessage(), $exception->getCode(), $exception
            );
        }
    }

    /**
     * @param int|string $analyzableId
     * @param string      $name
     * @param array       $eventProperties
     * @param Carbon|null $time
     * @return void
     */
    public function track($analyzableId, string $name, array $eventProperties, ?Carbon $time = null): void
    {
        try {
            $eventProperties = $this->transform($eventProperties);

            Rudder::track(array(
                'userId' => $analyzableId,
                'event' => $name,
                'properties' => $eventProperties,
                'timestamp' => $time ? $time->timestamp : Carbon::now()->timestamp
            ));
        } catch (Exception $exception) {
            throw new RuntimeException(
                'RudderStack: Event could not be tracked. Error code ' . $exception->getCode() .
                ' with message: ' . $exception->getMessage(), $exception->getCode(), $exception
            );
        }
    }

    /**
     * @return void
     */
    public function flush(): void
    {
        Rudder::flush();
    }
}
