<?php
namespace Aheadworks\Analytics\Gateway\Channels;

use Aheadworks\Analytics\Gateway\Channels\Transformers\CarbonToIsoStringTransformer;
use Aheadworks\Analytics\Gateway\Contracts\Data\ReservedProperties;
use Exception;
use Illuminate\Support\Carbon;
use RuntimeException;
use Segment;

class SegmentIo extends AbstractChannel
{
    /**
     * @param string   $write_key
     * @param string[] $transformers
     */
    public function __construct(string $write_key, array $transformers = []) {
        if (empty($write_key)) {
            throw new RuntimeException('Segment.io: [write_key] must be specified');
        }

        Segment::init($write_key);

        $this->setTransformers(CarbonToIsoStringTransformer::class, $transformers);
    }

    /**
     * @param int|string $analyzableId
     * @param array  $properties
     * @return void
     */
    public function identify($analyzableId, array $properties): void
    {
        try {
            $payload = [
                'userId' => $analyzableId,
            ];

            if (isset($properties[ReservedProperties::CREATED_AT])
                && (
                    $properties[ReservedProperties::CREATED_AT] instanceof \Carbon\Carbon
                    || $properties[ReservedProperties::CREATED_AT] instanceof \Illuminate\Support\Carbon
                )
            ) {
                $payload['timestamp'] = $properties[ReservedProperties::CREATED_AT]->timestamp;
            }

            $payload['traits'] = $this->transform($properties);

            Segment::identify($payload);
        } catch (Exception $exception) {
            throw new RuntimeException(
                'SegmentIO: Could not identify user. Error code ' . $exception->getCode() .
                ' with message: ' . $exception->getMessage(), $exception->getCode(), $exception
            );
        }
    }

    /**
     * @param int|string $analyzableId
     * @param string      $name
     * @param array       $eventProperties
     * @param Carbon|null $time
     * @return void
     */
    public function track($analyzableId, string $name, array $eventProperties, ?Carbon $time = null): void
    {
        try {
            Segment::track(array(
                'userId' => $analyzableId,
                'event' => $name,
                'properties' => $eventProperties,
                'timestamp' => $time ? $time->timestamp : Carbon::now()->timestamp
            ));
        } catch (Exception $exception) {
            throw new RuntimeException(
                'SegmentIO: Event could not be tracked. Error code ' . $exception->getCode() .
                ' with message: ' . $exception->getMessage(), $exception->getCode(), $exception
            );
        }
    }

    /**
     * @return void
     */
    public function flush(): void
    {
        Segment::flush();
    }
}
