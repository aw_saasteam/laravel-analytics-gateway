<?php
namespace Aheadworks\Analytics\Gateway\Channels;

use Aheadworks\Analytics\Gateway\ChannelManager;
use Aheadworks\Analytics\Gateway\Contracts\Channels\Channel;
use Aheadworks\Analytics\Gateway\Exceptions\ExceptionHandler;
use Illuminate\Support\Carbon;

class Stack implements Channel
{
    /**
     * @var string[]
     */
    private $channels;

    /**
     * @var ChannelManager
     */
    private $channelManager;

    /**
     * @var ExceptionHandler
     */
    private $errorHandler;

    /**
     * @param ChannelManager   $channelManager
     * @param ExceptionHandler $errorHandler
     * @param array            $channels
     */
    public function __construct(ChannelManager $channelManager, ExceptionHandler $errorHandler, array $channels)
    {
        $this->channelManager = $channelManager;
        $this->errorHandler = $errorHandler;
        $this->channels = $channels;
    }

    /**
     * @param int|string $analyzableId
     * @param array      $properties
     * @return void
     */
    public function identify($analyzableId, array $properties): void
    {
        foreach ($this->channels as $channel) {
            try {
                $this->channelManager->channel($channel)
                    ->identify($analyzableId, $properties);
            } catch (\Exception $exception) {
                $this->errorHandler->handle($exception);
                continue;
            }
        }
    }

    /**
     * @param int|string $analyzableId
     * @param string      $name
     * @param array       $eventProperties
     * @param Carbon|null $time
     * @return void
     */
    public function track($analyzableId, string $name, array $eventProperties, ?Carbon $time = null): void
    {
        foreach ($this->channels as $channel) {
            try {
                $this->channelManager->channel($channel)->track(
                    $analyzableId,
                    $name,
                    $eventProperties,
                    $time
                );
            } catch (\Exception $exception) {
                $this->errorHandler->handle($exception);
                continue;
            }
        }
    }

    /**
     * @return void
     */
    public function flush(): void
    {
        foreach ($this->channels as $channel) {
            try {
                $this->channelManager->channel($channel)->flush();
            } catch (\Exception $exception) {
                $this->errorHandler->handle($exception);
                continue;
            }
        }
    }
}
