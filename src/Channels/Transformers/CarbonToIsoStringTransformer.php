<?php
namespace Aheadworks\Analytics\Gateway\Channels\Transformers;

use Aheadworks\Analytics\Gateway\Contracts\Channels\PayloadTransformer;

class CarbonToIsoStringTransformer implements PayloadTransformer
{
    /**
     * @param array $payload
     * @return array
     */
    public function transform(array $payload): array
    {
        foreach ($payload as &$value) {
            if ($value instanceof \Carbon\Carbon
                || $value instanceof \Illuminate\Support\Carbon
            ) {
                $value = $value->toIso8601String();
            }
        }

        return $payload;
    }
}
