<?php
namespace Aheadworks\Analytics\Gateway\Channels\Transformers;

use Aheadworks\Analytics\Gateway\Contracts\Channels\PayloadTransformer;

class CarbonToUnixTimestampTransformer implements PayloadTransformer
{
    /**
     * @param array $payload
     * @return array
     */
    public function transform(array $payload): array
    {
        foreach ($payload as &$value) {
            if ($value instanceof \Carbon\Carbon
                || $value instanceof \Illuminate\Support\Carbon
            ) {
                $value = $value->timestamp;
            }
        }

        return $payload;
    }
}
