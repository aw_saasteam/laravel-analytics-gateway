<?php
namespace Aheadworks\Analytics\Gateway\Channels\Transformers;

use Aheadworks\Analytics\Gateway\Contracts\Channels\PayloadTransformer;
use Aheadworks\Analytics\Gateway\Contracts\Data\ReservedProperties;

class CustomerIoReservedPropertiesTransformer implements PayloadTransformer
{
    /**
     * @var string[]
     */
    protected $selfReservedToCustomerIoReservedMap = [
        ReservedProperties::CREATED_AT => 'created_at'
    ];

    /**
     * @param array $payload
     * @return array
     */
    public function transform(array $payload): array
    {
        $transformedPayload = [];

        foreach ($payload as $key => $value) {
            if (array_key_exists($key, $this->selfReservedToCustomerIoReservedMap)) {
                $key = $this->selfReservedToCustomerIoReservedMap[$key];
            }

            $transformedPayload[$key] = $value;
        }

        return $transformedPayload;
    }
}
