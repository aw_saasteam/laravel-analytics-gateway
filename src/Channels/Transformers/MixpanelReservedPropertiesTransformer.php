<?php
namespace Aheadworks\Analytics\Gateway\Channels\Transformers;

use Aheadworks\Analytics\Gateway\Contracts\Channels\PayloadTransformer;
use Aheadworks\Analytics\Gateway\Contracts\Data\ReservedProperties;

class MixpanelReservedPropertiesTransformer implements PayloadTransformer
{
    /**
     * @var string[]
     */
    protected $selfReservedToMixpanelReservedMap = [
        ReservedProperties::CREATED_AT => 'created'
    ];

    /**
     * @var string[]
     */
    protected $mixpanelReservedProperties = [
        'email', 'name', 'phone', 'first_name', 'last_name',
        'city', 'region', 'country_code', 'geo_source', 'timezone',
        'created', 'last_seen',
    ];

    /**
     * @param array $payload
     * @return array
     */
    public function transform(array $payload): array
    {
        $transformedPayload = [];

        foreach ($payload as $key => $value) {
            if (array_key_exists($key, $this->selfReservedToMixpanelReservedMap)) {
                $key = $this->selfReservedToMixpanelReservedMap[$key];
            }

            if (in_array($key, $this->mixpanelReservedProperties)) {
                $key = '$' . $key;
            }

            $transformedPayload[$key] = $value;
        }

        return $transformedPayload;
    }
}
