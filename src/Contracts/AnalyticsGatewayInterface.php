<?php
namespace Aheadworks\Analytics\Gateway\Contracts;

use Aheadworks\Analytics\Gateway\Contracts\Data\Model\Analyzable;
use Illuminate\Support\Carbon;

interface AnalyticsGatewayInterface
{
    /**
     * @param string $name
     * @return $this
     */
    public function channel(string $name);

    /**
     * @param Analyzable           $analyzable
     * @param array<string, mixed> $properties
     * @param Carbon|null          $createdAt
     * @return void
     */
    public function identify(Analyzable $analyzable, array $properties, ?Carbon $createdAt = null);

    /**
     * @param Analyzable           $analyzable
     * @param array<string, mixed> $properties
     * @return void
     */
    public function updateUser(Analyzable $analyzable, array $properties);

    /**
     * @param Analyzable           $analyzable
     * @param string               $event
     * @param array<string, mixed> $eventProperties
     * @param Carbon|null          $time
     * @return void
     */
    public function track(Analyzable $analyzable, string $event, array $eventProperties, ?Carbon $time = null): void;

    /**
     * @return void
     */
    public function flush();
}
