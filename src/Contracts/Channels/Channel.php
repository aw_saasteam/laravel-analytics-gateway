<?php
namespace Aheadworks\Analytics\Gateway\Contracts\Channels;

use Illuminate\Support\Carbon;

interface Channel
{
    const DEBUG = 'debug';
    const STACK = 'stack';
    const MIXPANEL = 'mixpanel-channel';
    const CUSTOMER_IO = 'customer_io-channel';
    const SEGMENT_IO = 'segment-channel';
    const RUDDERSTACK = 'rudderstack-channel';

    /**
     * @param int|string $analyzableId
     * @param array      $properties
     * @return void
     */
    public function identify($analyzableId, array $properties): void;

    /**
     * @param int|string $analyzableId
     * @param string $name
     * @param array  $eventProperties
     * @param Carbon $time
     * @return void
     */
    public function track($analyzableId, string $name, array $eventProperties, Carbon $time): void;

    /**
     * @return void
     */
    public function flush(): void;
}
