<?php
namespace Aheadworks\Analytics\Gateway\Contracts\Channels;

interface PayloadTransformer
{
    /**
     * @param array $payload
     * @return array
     */
    public function transform(array $payload): array;
}
