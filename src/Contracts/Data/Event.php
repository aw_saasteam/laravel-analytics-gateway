<?php
namespace Aheadworks\Analytics\Gateway\Contracts\Data;

use Aheadworks\Analytics\Gateway\Contracts\Data\Model\Analyzable;
use Aheadworks\Analytics\Gateway\Data\AnalyzableProperties;
use Aheadworks\Analytics\Gateway\Events\EventDefinition;

interface Event
{
    /**
     * @return Analyzable
     */
    public function getAnalyzable(): Analyzable;

    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @return array
     */
    public function getProperties(): array;

    /**
     * @return AnalyzableProperties
     */
    public function getAnalyzableProperties(): AnalyzableProperties;

    /**
     * @return EventDefinition
     */
    public function getDefinition(): EventDefinition;
}
