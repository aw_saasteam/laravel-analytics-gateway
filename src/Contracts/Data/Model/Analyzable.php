<?php
namespace Aheadworks\Analytics\Gateway\Contracts\Data\Model;

interface Analyzable
{
    public const REQUIRED_ANALYZABLE_PROPERTY_SCOPE = 'analyzable_properties';

    /**
     * @return int|string
     */
    public function getAnalyzableId();

    /**
     * @return int|string
     */
    public function getSystemId();
}
