<?php
namespace Aheadworks\Analytics\Gateway\Contracts\Data;

interface Property
{
    public const COUNTRY = 'country';
    public const SHOPIFY_PLAN = 'shopify_plan';
    public const IS_BILLABLE = 'is_billable';
    public const SUBSCRIPTION_TYPE = 'subscription_type';
    public const IS_LIVE = 'is_live';
    public const IS_TRIAL = 'is_trial';
    public const IS_INSTALLED = 'is_installed';
    public const REMAINING_TRIAL_DAYS = 'remaining_trial_days';
    public const STORE_CREATED_AT = 'store_created_at';
    public const SUBSCRIPTION_PLAN = 'subscription_plan';
    public const IS_OPEN = 'is_open';
    public const TRIAL_DAYS_LEFT = 'trials_days_left';
    public const PLAN_CHANGE_CONFIRMED_AT = 'plan_change_confirmed_at';
    public const DATE_OF_FIRST_INSTALL = 'date_of_first_install';
    public const DATE_OF_LAST_INSTALL = 'date_of_last_install';
    public const DATE_OF_LAST_UNINSTALL = 'date_of_last_uninstall';

    public const CHARGE_TYPE = 'charge_type';
    public const IS_FIRST_CHARGE = 'is_first_charge';
    public const CHARGE_CROSS_AMOUNT = 'charge_cross_amount';
    public const CHARGE_ID = 'charge_id';
    public const REVENUE = 'revenue';

    public const URL = 'url';
    public const PATH = 'path';
    public const DEVICE = 'device';

    public const PLAN_BEFORE = 'plan_before';
    public const PLAN_AFTER = 'plan_after';
    public const CAMPAIGN_NAME = 'campaign_name';
    public const COUPON_CODE = 'coupon_code';
    public const UTM = 'utm';

    public const SOURCE = 'source';
    public const ELEMENT = 'element';
    public const CAMPAIGN = 'campaign';

    public const FROM = 'from';
    public const TO = 'to';

    public const ACTION = 'action';
}
