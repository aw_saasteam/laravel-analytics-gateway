<?php
namespace Aheadworks\Analytics\Gateway\Contracts\Data\Property;

use Aheadworks\Analytics\Gateway\Contracts\Data\Model\Analyzable;
use Aheadworks\Analytics\Gateway\Data\Property\PropertiesCollection;

interface GroupedResolver extends Resolver
{
    /**
     * @param Analyzable $analyzable
     * @return PropertiesCollection
     */
    public function resolve(Analyzable $analyzable): PropertiesCollection;
}
