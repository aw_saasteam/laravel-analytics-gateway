<?php
namespace Aheadworks\Analytics\Gateway\Contracts\Data\Property;

use Aheadworks\Analytics\Gateway\Contracts\Data\Model\Analyzable;
use Illuminate\Support\Carbon;

interface Resolver
{
    /**
     * @param Analyzable $analyzable
     * @return int|float|string|bool|Carbon
     */
    public function resolve(Analyzable $analyzable);
}
