<?php
namespace Aheadworks\Analytics\Gateway\Contracts\Data;

interface ReservedProperties
{
    public const CREATED_AT = 'created_at';
}
