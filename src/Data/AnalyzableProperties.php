<?php
namespace Aheadworks\Analytics\Gateway\Data;

class AnalyzableProperties
{
    /**
     * @var array<string, mixed>
     */
    protected $properties;

    /**
     * @var array<int, string>
     */
    protected $unresolvedProperties;

    /**
     * @param array<string, mixed> $properties
     * @param array<int, string> $propertiesToResolve
     */
    public function __construct(
        array $properties = [],
        array $propertiesToResolve = []
    ) {
        $this
            ->setProperties($properties)
            ->setPropertiesToResolve($propertiesToResolve);
    }

    /**
     * @param array<string, mixed> $properties
     * @param array<int, string> $propertiesToResolve
     */
    public static function create(
        array $properties = [],
        array $propertiesToResolve = []
    ): self {
        return new self($properties, $propertiesToResolve);
    }

    /**
     * Set properties
     *
     * @param array $properties
     * @return $this
     */
    public function setProperties(array $properties): self
    {
        $this->properties = $properties;
        return $this;
    }

    /**
     * Set properties to resolve
     *
     * @param array<int, string> $unresolvedProperties
     * @return $this
     */
    public function setPropertiesToResolve(array $unresolvedProperties): self
    {
        $this->unresolvedProperties = $unresolvedProperties;
        return $this;
    }

    /**
     * Add properties
     *
     * @param array<string, mixed> $properties
     * @return $this
     */
    public function addProperties(array $properties): self
    {
        foreach ($properties as $name => $value) {
            $this->addProperty($name, $value);
        }

        return $this;
    }

    /**
     * Add properties to resolve
     *
     * @param array<int, string> $properties
     * @return $this
     */
    public function addPropertiesToResolve(array $properties): self
    {
        $this->unresolvedProperties = array_unique(
            array_merge(
                $this->unresolvedProperties,
                $properties
            )
        );

        return $this;
    }

    /**
     * Add property
     *
     * @param string $name
     * @param mixed $value
     *
     * @return $this
     */
    public function addProperty(string $name, $value): self
    {
        if ($index = array_search($name, $this->unresolvedProperties)) {
            unset($this->unresolvedProperties[$index]);
        }

        $this->properties[$name] = $value;
        return $this;
    }

    /**
     * Add property to resolve
     *
     * @param string $name
     *
     * @return $this
     */
    public function addPropertyToResolve(string $name): self
    {
        if (!array_search($name, $this->unresolvedProperties)) {
            $this->unresolvedProperties[] = $name;
        }

        return $this;
    }

    /**
     * Get properties
     *
     * @return array<string, mixed>
     */
    public function getProperties(): array
    {
        return $this->properties;
    }

    /**
     * Get unresolved properties
     *
     * @return array<int, string>
     */
    public function getUnresolvedProperties(): array
    {
        return $this->unresolvedProperties;
    }

    /**
     * Has properties
     *
     * @return bool
     */
    public function hasProperties(): bool
    {
        return (bool)$this->properties;
    }

    /**
     * Has unresolved properties
     *
     * @return bool
     */
    public function hasUnresolvedProperties(): bool
    {
        return (bool)$this->unresolvedProperties;
    }
}
