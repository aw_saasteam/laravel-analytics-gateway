<?php
namespace Aheadworks\Analytics\Gateway\Data;

use Aheadworks\Analytics\Gateway\Contracts\Data\Model\Analyzable;
use Aheadworks\Analytics\Gateway\Contracts\Data\Property\GroupedResolver;
use Aheadworks\Analytics\Gateway\Contracts\Data\Property\Resolver;
use Aheadworks\Analytics\Gateway\Data\Property\PropertiesCollection;
use Aheadworks\Analytics\Gateway\Data\Property\TypeChecker;
use Aheadworks\Analytics\Gateway\Exceptions\ScopeNotDefinedException;
use Aheadworks\Analytics\Gateway\Exceptions\TypeMismatchException;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use InvalidArgumentException;
use TypeError;

class PropertiesResolver
{
    /**
     * @var TypeChecker
     */
    private $typeChecker;

    /**
     * @var array<string, Resolver|string>
     */
    private $propertyResolvers;

    /**
     * @param TypeChecker $typeChecker
     * @param array|null $propertyResolvers
     */
    public function __construct(TypeChecker $typeChecker, array $propertyResolvers = null)
    {
        $this->typeChecker = $typeChecker;
        $this->propertyResolvers = $propertyResolvers ?? app()['config']['analytics.property_resolvers'] ?? [];
    }

    /**
     * @param Analyzable $analyzable
     * @return PropertiesCollection
     * @throws BindingResolutionException
     */
    public function all(Analyzable $analyzable): PropertiesCollection
    {
        $collection = new PropertiesCollection();

        foreach ($this->propertyResolvers as $propertyName => $resolverName) {
            $this->getValue($analyzable, $propertyName, $collection);
        }

        return $collection;
    }

    /**
     * @param Analyzable $analyzable
     * @param string[] $propertyNames
     * @return PropertiesCollection
     * @throws BindingResolutionException
     */
    public function getMany(Analyzable $analyzable, array $propertyNames): PropertiesCollection
    {
        $collection = new PropertiesCollection();

        foreach ($propertyNames as $propertyName) {
            if (!array_key_exists($propertyName, $this->propertyResolvers)) {
                throw new InvalidArgumentException(
                    "Analytics: [$propertyName] property not defined in analytics.php config file"
                );
            }

            $this->getValue($analyzable, $propertyName, $collection);
        }

        return $collection->only($propertyNames);
    }

    /**
     * @param Analyzable $analyzable
     * @param string $propertyName
     * @return bool|float|Carbon|int|string
     * @throws BindingResolutionException
     */
    public function getOne(Analyzable $analyzable, string $propertyName)
    {
        if (!array_key_exists($propertyName, $this->propertyResolvers)) {
            throw new InvalidArgumentException(
                "[$propertyName] property not defined in analytics.php config file"
            );
        }

        return $this->getValue($analyzable, $propertyName, new PropertiesCollection());
    }

    /**
     * @param Analyzable $analyzable
     * @param string $name
     * @return PropertiesCollection
     * @throws BindingResolutionException
     */
    public function scope(Analyzable $analyzable, string $name): PropertiesCollection
    {
        $propertyNames = $this->getPropertyNamesByScopeName($name);

        return $this->getMany($analyzable, $propertyNames);
    }

    /**
     * @param string $name
     * @return array
     */
    public static function getPropertyNamesByScopeName(string $name): array
    {
        $scopes = Config::get('analytics.scopes', []);

        if (!array_key_exists($name, $scopes)) {
            throw new ScopeNotDefinedException(
                "[$name] scope not defined in analytics.php config file"
            );
        }

        return $scopes[$name];
    }

    /**
     * @param Analyzable $analyzable
     * @param string $propertyName
     * @param PropertiesCollection $collection
     * @return bool|float|Carbon|int|string
     * @throws BindingResolutionException
     */
    private function getValue(Analyzable $analyzable, string $propertyName, PropertiesCollection $collection)
    {
        if ($collection->has($propertyName)) {
            return $collection->get($propertyName);
        }

        $resolver = $this->getResolver($propertyName);

        try {
            if ($resolver instanceof GroupedResolver) {
                $valueCollection = $this->resolveValueWithCache($analyzable, $resolver);
                foreach ($valueCollection as $value) {
                    $this->typeChecker->check($value);
                }
                $collection->merge($valueCollection);
                $value = $valueCollection->get($propertyName);
            } else {
                $value = $this->resolveValueWithCache($analyzable, $resolver);
                $this->typeChecker->check($value);
                $collection->put($propertyName, $value);
            }
        } catch (TypeMismatchException $exception) {
            throw new TypeError('Property ['. $propertyName .'] has unsupported type', 0, $exception);
        }

        return $value;
    }

    /**
     * @param Analyzable $analyzable
     * @param Resolver   $resolver
     * @return bool|float|Carbon|int|string|PropertiesCollection
     */
    private function resolveValueWithCache(Analyzable $analyzable, Resolver $resolver)
    {
        $cacheKey = $analyzable->getSystemId() . get_class($resolver);
        $value = Cache::get($cacheKey);

        if (!$value) {
            $value = $resolver->resolve($analyzable);
            Cache::put($cacheKey, $value, 30);
        }

        return $value;
    }

    /**
     * @param string $propertyName
     * @return Resolver
     * @throws BindingResolutionException
     */
    private function getResolver(string $propertyName): Resolver
    {
        if (is_string($this->propertyResolvers[$propertyName])) {
            $this->propertyResolvers[$propertyName] = app($this->propertyResolvers[$propertyName]);

            if (!$this->propertyResolvers[$propertyName] instanceof Resolver) {
                throw new InvalidArgumentException(
                    "[$propertyName] property resolver does not implement contract " . Resolver::class
                );
            }
        }

        return $this->propertyResolvers[$propertyName];
    }
}
