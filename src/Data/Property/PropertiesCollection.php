<?php
namespace Aheadworks\Analytics\Gateway\Data\Property;

use Illuminate\Support\Collection;

class PropertiesCollection extends Collection
{
    /**
     * @param PropertiesCollection $items
     * @return $this
     */
    public function merge($items)
    {
        if ($items instanceof PropertiesCollection) {
            foreach ($items as $key => $value) {
                $this->put($key, $value);
            }
            return $this;
        } else {
            return parent::merge($items);
        }
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return $this->items;
    }

    /**
     * @return array
     */
    public function keysAsArray(): array
    {
        return array_keys($this->items);
    }
}
