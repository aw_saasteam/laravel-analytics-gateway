<?php
namespace Aheadworks\Analytics\Gateway\Data\Property\Resolver;

use Aheadworks\Analytics\Gateway\Contracts\Data\Model\Analyzable;
use Aheadworks\Analytics\Gateway\Contracts\Data\Property\Resolver;
use Illuminate\Support\Carbon;

class DummyResolver implements Resolver
{
    /**
     * @param Analyzable $analyzable
     * @return bool|float|Carbon|int|string
     */
    public function resolve(Analyzable $analyzable)
    {
        throw new \RuntimeException('You need to implement your own property resolver instead this dummy');
    }
}
