<?php
namespace Aheadworks\Analytics\Gateway\Data\Property;

use Aheadworks\Analytics\Gateway\Exceptions\TypeMismatchException;
use Carbon\Carbon as BaseCarbon;
use Illuminate\Support\Carbon as IlluminateCarbon;

class TypeChecker
{
    /**
     * @param $propertyValue
     * @return bool
     * @throws TypeMismatchException
     */
    public function check($propertyValue): bool
    {
        if (
            is_int($propertyValue) ||
            is_string($propertyValue) ||
            is_float($propertyValue) ||
            is_bool($propertyValue) ||
            is_null($propertyValue) ||
            is_array($propertyValue) ||
            $propertyValue instanceof IlluminateCarbon ||
            $propertyValue instanceof BaseCarbon
        ) {
            return true;
        }

        throw new TypeMismatchException('Analytics: ['. gettype($propertyValue) .'] is not supported type');
    }
}
