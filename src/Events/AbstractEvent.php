<?php
namespace Aheadworks\Analytics\Gateway\Events;

use Aheadworks\Analytics\Gateway\Contracts\Data\Event;
use Aheadworks\Analytics\Gateway\Contracts\Data\Model\Analyzable;
use Aheadworks\Analytics\Gateway\Data\AnalyzableProperties;
use Aheadworks\Analytics\Gateway\Data\Property\TypeChecker;

abstract class AbstractEvent implements Event
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var array
     */
    protected $properties;

    /**
     * @var AnalyzableProperties
     */
    protected $analyzableProperties;

    /**
     * @var EventDefinition
     */
    private $definition;

    /**
     * @var Analyzable
     */
    private $analyzable;

    /**
     * @var TypeChecker
     */
    private $typeChecker;

    /**
     * @param Analyzable $analyzable
     * @param string $name
     * @param array $properties
     * @param EventDefinition $definition
     * @param AnalyzableProperties|null $analyzableProperties
     */
    public function __construct(
        Analyzable $analyzable,
        string $name,
        array $properties,
        EventDefinition $definition,
        AnalyzableProperties $analyzableProperties
    ) {
        $this->analyzable = $analyzable;
        $this->name = $name;
        $this->properties = $properties;
        $this->definition = $definition;
        $this->typeChecker = app(TypeChecker::class);
        $this->analyzableProperties = $analyzableProperties;

        array_walk($properties, function ($value) {
            $this->typeChecker->check($value);
        });
    }

    /**
     * @return Analyzable
     */
    public function getAnalyzable(): Analyzable
    {
        return $this->analyzable;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return array
     */
    public function getProperties(): array
    {
        return $this->properties;
    }

    /**
     * @return AnalyzableProperties
     */
    public function getAnalyzableProperties(): AnalyzableProperties
    {
        return $this->analyzableProperties;
    }

    /**
     * @return EventDefinition
     */
    public function getDefinition(): EventDefinition
    {
        return $this->definition;
    }

    /**
     * @param string $name
     * @param mixed  $value
     * @return $this
     */
    public function setProperty(string $name, $value): AbstractEvent
    {
        $this->typeChecker->check($value);
        $this->properties[$name] = $value;

        return $this;
    }
}
