<?php
namespace Aheadworks\Analytics\Gateway\Events\Common;

class EventList
{
    public const APP_INSTALLED = 'app_installed';
    public const APP_UNINSTALLED = 'app_uninstalled';
    public const STORE_CLOSED = 'store_closed';
    public const STORE_REOPENED = 'store_reopened';
    public const STORE_CHARGED = 'store_charged';
    public const PAGE_VIEWED = 'page_viewed';
    public const COUPON_APPLIED = 'coupon_applied';
    public const UPGRADE_TO_UNLOCK_DISPLAYED = 'upgrade_to_unlock_displayed';
    public const UPGRADE_CLICKED = 'upgrade_clicked';
    public const SWITCHED_TO_MONTHLY_OR_ANNUAL_VIEW = 'switched_to_monthly_or_annual_view';
    public const PLAN_SELECTED = 'plan_selected';
    public const PLAN_CHANGED = 'plan_changed';
    public const RATE_US = 'rate_us';

    public static function getList(): array
    {
        return [
            self::APP_INSTALLED,
            self::APP_UNINSTALLED,
            self::STORE_CLOSED,
            self::STORE_REOPENED,
            self::STORE_CHARGED,
            self::PAGE_VIEWED,
            self::COUPON_APPLIED,
            self::UPGRADE_TO_UNLOCK_DISPLAYED,
            self::UPGRADE_CLICKED,
            self::SWITCHED_TO_MONTHLY_OR_ANNUAL_VIEW,
            self::PLAN_SELECTED,
            self::PLAN_CHANGED,
            self::RATE_US
        ];
    }
}
