<?php
namespace Aheadworks\Analytics\Gateway\Events;

use Aheadworks\Analytics\Gateway\Contracts\Data\Model\Analyzable;
use Aheadworks\Analytics\Gateway\Data\AnalyzableProperties;
use Aheadworks\Analytics\Gateway\Data\PropertiesResolver;
use Aheadworks\Analytics\Gateway\Exceptions\EventNotDefinedException;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Support\Facades\Config;

class EventCreator
{
    /**
     * @var PropertiesResolver
     */
    private $propertiesResolver;

    /**
     * @param PropertiesResolver $propertiesResolver
     */
    public function __construct(
        PropertiesResolver $propertiesResolver
    ) {
        $this->propertiesResolver = $propertiesResolver;
    }

    /**
     * Create event
     *
     * @param Analyzable $analyzable
     * @param string $eventName
     * @param array $eventData
     * @param AnalyzableProperties|null $analyzableProperties
     *
     * @return AbstractEvent
     * @throws BindingResolutionException
     */
    public function create(
        Analyzable $analyzable,
        string $eventName,
        array $eventData = [],
        AnalyzableProperties $analyzableProperties = null
    ): AbstractEvent {
        $eventConfig = Config::get("analytics.events.$eventName");
        if (!$eventConfig) {
            throw new EventNotDefinedException("Event $eventName not defined.");
        }

        $eventDefinition = new EventDefinition($eventConfig);
        $properties = $this->getEventProperties($analyzable, $eventDefinition, $eventData);
        $analyzableProperties = $analyzableProperties ?? AnalyzableProperties::create();

        if ($analyzableProperties->hasUnresolvedProperties()) {
            $resolvedAnalyzableProperties = $this->propertiesResolver->getMany(
                $analyzable,
                $analyzableProperties->getUnresolvedProperties()
            )->toArray();
            $analyzableProperties->addProperties($resolvedAnalyzableProperties);
        }

        return app(
            $eventDefinition->getClassName(),
            [
                'analyzable' => $analyzable,
                'name' => $eventName,
                'properties' => $properties,
                'definition' => $eventDefinition,
                'analyzableProperties' => $analyzableProperties
            ]
        );
    }

    /**
     * Get event properties
     *
     * @param Analyzable $analyzable
     * @param EventDefinition $eventDefinition
     * @param array $eventData
     *
     * @return array
     * @throws BindingResolutionException
     */
    private function getEventProperties(
        Analyzable $analyzable,
        EventDefinition $eventDefinition,
        array $eventData
    ): array {
        $propertiesToResolve = array_filter(
            $eventDefinition->getEventPropertyNames(),
            function ($property) use ($eventData) {
                return !isset($eventData[$property]);
            }
        );

        foreach ($eventDefinition->getMapping() as $search => $replacement) {
            $index = array_search($search, $propertiesToResolve);
            if ($index !== false) {
                $propertiesToResolve[$index] = $replacement;
            }
        }

        $properties = array_merge(
            $this->propertiesResolver->getMany($analyzable, $propertiesToResolve)->toArray(),
            $eventData
        );

        foreach ($eventDefinition->getMapping() as $replacement => $search) {
            if (isset($properties[$search])) {
                $properties[$replacement] = $properties[$search];
                unset($properties[$search]);
            }
        }

        return $properties;
    }
}
