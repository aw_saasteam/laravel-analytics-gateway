<?php
namespace Aheadworks\Analytics\Gateway\Events;

use Aheadworks\Analytics\Gateway\Data\PropertiesResolver;
use Illuminate\Support\Facades\Config;

class EventDefinition
{
    /**
     * @var string
     */
    protected $className;

    /**
     * @var array
     */
    protected $properties;

    /**
     * @var array
     */
    protected $scopes;

    /**
     * @var array
     */
    protected $eventPropertyNames;

    /**
     * @var array<string, string>
     */
    protected $mapping;

    /**
     * @param array $eventConfig
     */
    public function __construct(array $eventConfig = [])
    {
        $this->className = $eventConfig['class_name'] ?? Config::get('analytics.default.event_class_name');
        $this->properties = $eventConfig['properties'] ?? [];
        $this->scopes = $eventConfig['scopes'] ?? [];
        $this->mapping = $eventConfig['mapping'] ?? [];
        $this->eventPropertyNames = $this->getEventPropertyNames();
    }

    /**
     * Get class name
     *
     * @return string
     */
    public function getClassName(): string
    {
        return $this->className;
    }

    /**
     * Get properties
     *
     * @return string[]
     */
    public function getProperties(): array
    {
        return $this->properties;
    }

    /**
     * Get all properties including those specified through scopes
     *
     * @return string[]
     */
    public function getEventPropertyNames(): array
    {
        if (!$this->eventPropertyNames) {
            $scopeProperties = [];

            foreach ($this->getScopes() as $scope) {
                $scopeProperties = array_merge(
                    PropertiesResolver::getPropertyNamesByScopeName($scope),
                    $scopeProperties
                );
            }
            $this->eventPropertyNames = array_merge($this->getProperties(), $scopeProperties);
        }

        return $this->eventPropertyNames;
    }

    /**
     * Get scopes
     *
     * @return string[]
     */
    public function getScopes(): array
    {
        return $this->scopes;
    }

    /**
     * Get mapping
     *
     * @return array<string, string>
     */
    public function getMapping(): array
    {
        return $this->mapping;
    }
}
