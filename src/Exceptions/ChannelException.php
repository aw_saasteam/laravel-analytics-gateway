<?php
namespace Aheadworks\Analytics\Gateway\Exceptions;

use Illuminate\Support\Facades\Config;
use RuntimeException;
use Throwable;

class ChannelException extends RuntimeException
{
    /**
     * @param Throwable $throwable
     * @return self
     */
    public static function create(Throwable $throwable): self
    {
        return new static($throwable->getMessage(), $throwable->getCode(), $throwable);
    }
}
