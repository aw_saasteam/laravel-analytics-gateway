<?php
namespace Aheadworks\Analytics\Gateway\Exceptions\CustomerIo;

use RuntimeException;

class IdentifiersNotValidException extends RuntimeException
{
}
