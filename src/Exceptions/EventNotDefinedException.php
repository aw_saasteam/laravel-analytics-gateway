<?php
namespace Aheadworks\Analytics\Gateway\Exceptions;

use RuntimeException;

class EventNotDefinedException extends RuntimeException
{
}
