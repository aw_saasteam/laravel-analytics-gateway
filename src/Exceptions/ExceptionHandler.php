<?php
namespace Aheadworks\Analytics\Gateway\Exceptions;

use Illuminate\Support\Facades\Log;
use Psr\Log\LoggerInterface;

class ExceptionHandler
{
    /**
     * @var LoggerInterface
     */
    protected $logger;
    
    /**
     * @var bool
     */
    protected $isReporting;

    /**
     * @var bool
     */
    protected $isLogging;

    /**
     * @var string
     */
    protected $logChannel;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
        $this->isReporting = config('analytics.errors.reporting', true);
        $this->isLogging = config('analytics.errors.logging', false);
        $this->logChannel = config('analytics.errors.log_channel', null) ?? config('logging.default');
    }

    /**
     * @param \Throwable $throwable
     * @return void
     */
    public function handle(\Throwable $throwable)
    {
        if ($this->isReporting) {
            report($throwable);
        }

        if ($this->isLogging) {
            $this->logger->channel($this->logChannel)->error(
                $throwable->getMessage(),
                [
                    'file' => $throwable->getFile(),
                    'line' => $throwable->getLine(),
                    'exception' => get_class($throwable)
                ]
            );
        }
    }
}
