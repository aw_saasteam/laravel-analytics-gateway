<?php
namespace Aheadworks\Analytics\Gateway\Exceptions;

use RuntimeException;

class ScopeNotDefinedException extends RuntimeException
{
}
