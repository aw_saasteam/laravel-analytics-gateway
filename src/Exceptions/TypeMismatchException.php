<?php
namespace Aheadworks\Analytics\Gateway\Exceptions;

use RuntimeException;

class TypeMismatchException extends RuntimeException
{
}
