<?php
namespace Aheadworks\Analytics\Gateway\Facades;

use Aheadworks\Analytics\Gateway\Contracts\Data\Event;
use Aheadworks\Analytics\Gateway\Contracts\Data\Model\Analyzable;
use Aheadworks\Analytics\Gateway\Data\PropertiesResolver;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Facade;

/**
 * @see \Aheadworks\Analytics\Gateway\Service
 *
 * @method static \Aheadworks\Analytics\Gateway\Contracts\AnalyticsGatewayInterface channel(string $name)
 * @method static void identify(Analyzable $analyzable, array $properties)
 * @method static void updateProperties(Analyzable $analyzable, array $properties)
 * @method static void track(Analyzable $analyzable, string $event, array $eventProperties, ?Carbon $time = null)
 * @method static void flush()
 */
class Analytics extends Facade
{
    protected static function getFacadeAccessor()
    {
        return \Aheadworks\Analytics\Gateway\Contracts\AnalyticsGatewayInterface::class;
    }
}
