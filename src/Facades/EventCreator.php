<?php
namespace Aheadworks\Analytics\Gateway\Facades;

use Aheadworks\Analytics\Gateway\Contracts\Data\Model\Analyzable;
use Aheadworks\Analytics\Gateway\Data\AnalyzableProperties;
use Aheadworks\Analytics\Gateway\Events\AbstractEvent;
use Illuminate\Support\Facades\Facade;

/**
 * @see \Aheadworks\Analytics\Gateway\Events\EventCreator
 *
 * @method static AbstractEvent create(Analyzable $analyzable, string $eventName, array $eventData = [], AnalyzableProperties $analyzableProperties = null)
 */
class EventCreator extends Facade
{
    protected static function getFacadeAccessor()
    {
        return \Aheadworks\Analytics\Gateway\Events\EventCreator::class;
    }
}
