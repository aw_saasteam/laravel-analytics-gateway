<?php
namespace Aheadworks\Analytics\Gateway\Notifications;

use Aheadworks\Analytics\Gateway\Exceptions\ChannelException;
use Illuminate\Notifications\Notification;
use NotificationChannels\Discord\DiscordChannel;
use NotificationChannels\Discord\DiscordMessage;

class DiscordNotification extends Notification
{
    /**
     * @param ChannelException $exception
     * @return string[]
     */
    public function via(ChannelException $exception)
    {
        return [DiscordChannel::class];
    }

    /**
     * @param ChannelException $exception
     * @return DiscordMessage
     */
    public function toDiscord(ChannelException $exception)
    {
        return DiscordMessage::create($exception->getMessage());
    }
}
