<?php
namespace Aheadworks\Analytics\Gateway;

use Aheadworks\Analytics\Gateway\Contracts\Data\Model\Analyzable;
use Aheadworks\Analytics\Gateway\Contracts\Data\ReservedProperties;
use Aheadworks\Analytics\Gateway\Data\Property\TypeChecker;
use Aheadworks\Analytics\Gateway\Exceptions\ExceptionHandler;
use Aheadworks\Analytics\Gateway\Exceptions\TypeMismatchException;
use Exception;
use Illuminate\Support\Carbon;

class Service implements Contracts\AnalyticsGatewayInterface
{
    /**
     * @var ChannelManager
     */
    private $channelManager;

    /**
     * @var string|null
     */
    private $channel;

    /**
     * @var array
     */
    private $forkedObjects = [];

    /**
     * @var TypeChecker
     */
    private $typeChecker;

    /**
     * @var ExceptionHandler
     */
    private $errorHandler;

    /**
     * @param ChannelManager   $channelManager
     * @param TypeChecker      $typeChecker
     * @param ExceptionHandler $errorHandler
     * @param string|null      $channel
     */
    public function __construct(
        ChannelManager $channelManager,
        TypeChecker $typeChecker,
        ExceptionHandler $errorHandler,
        string $channel = null
    ) {
        $this->channelManager = $channelManager;
        $this->typeChecker = $typeChecker;
        $this->errorHandler = $errorHandler;
        $this->channel = $channel;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function channel(string $name)
    {
        if ($name != $this->channel) {
            if (!isset($this->forkedObjects[$name])) {
                $this->forkedObjects[$name] = new self(
                    $this->channelManager,
                    $this->typeChecker,
                    $this->errorHandler,
                    $name
                );
            }

            return $this->forkedObjects[$name];
        }

        return $this;
    }

    /**
     * @param Analyzable  $analyzable
     * @param array       $properties
     * @param Carbon|null $createdAt
     * @return void
     */
    public function identify(Analyzable $analyzable, array $properties, ?Carbon $createdAt = null)
    {
        if ($createdAt) {
            $properties[ReservedProperties::CREATED_AT] = $createdAt;
        }

        try {
            $this->checkPropertiesType($properties);

            $this->channelManager->channel($this->channel)->identify(
                $analyzable->getAnalyzableId(),
                $properties
            );

        } catch (Exception $exception) {
            $this->errorHandler->handle($exception);
        }
    }

    /**
     * @param Analyzable           $analyzable
     * @param array<string, mixed> $properties
     * @return void
     */
    public function updateUser(Analyzable $analyzable, array $properties)
    {
        $this->identify($analyzable, $properties);
    }

    /**
     * @param Analyzable           $analyzable
     * @param string               $event
     * @param array<string, mixed> $eventProperties
     * @param Carbon|null          $time
     * @return void
     */
    public function track(Analyzable $analyzable, string $event, array $eventProperties, ?Carbon $time = null): void
    {
        try {
            $this->checkPropertiesType($eventProperties);

            $this->channelManager->channel($this->channel)->track(
                $analyzable->getAnalyzableId(),
                $event,
                $eventProperties,
                $time ?: Carbon::now()
            );

        } catch (Exception $exception) {
            $this->errorHandler->handle($exception);
        }
    }

    /**
     * @return void
     */
    public function flush()
    {
        try {
            foreach ($this->channelManager->getActivatedChannels() as $channel) {
                $channel->flush();
            }
        } catch (Exception $exception) {
            $this->errorHandler->handle($exception);
        }
    }

    /**
     * @param array $properties
     * @return void
     */
    protected function checkPropertiesType(array $properties)
    {
        foreach ($properties as $key => &$value) {
            try {
                $this->typeChecker->check($value);
            } catch (TypeMismatchException $exception) {
                throw new TypeMismatchException(
                    'Analytics: property ['. $key .'] has not supported type ['. gettype($value) .']');
            }
        }
    }
}
