<?php
namespace Aheadworks\Analytics\Gateway;

use Aheadworks\Analytics\Gateway\Contracts\AnalyticsGatewayInterface;
use Aheadworks\Analytics\Gateway\Events\EventCreator;
use Aheadworks\Analytics\Gateway\Data\PropertiesResolver;
use Aheadworks\Analytics\Gateway\Data\Property\TypeChecker;
use Aheadworks\Analytics\Gateway\Facades\Analytics;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Queue;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{

    public $singletons = [
        Service::class => Service::class,
        ChannelManager::class => ChannelManager::class,
        EventCreator::class => EventCreator::class,
        PropertiesResolver::class => PropertiesResolver::class,
        TypeChecker::class => TypeChecker::class
    ];

    /**
     * @return void
     */
    public function register()
    {
        $this->app->bind(AnalyticsGatewayInterface::class, function ($app) {
            return $app->make(Service::class);
        });
    }

    /**
     * Bootstrap package services.
     *
     * @return void
     */
    public function boot()
    {
        // Setup config publishing
        $this->publishes([
            __DIR__.'/../config/analytics.php' => config_path('analytics.php')
        ], 'config');

        // Send deferred tracking events to analytics after the response has been sent.
        $this->app->terminating(function (Application $app) {
            if (!$app->runningInConsole()) {
                Analytics::flush();
            }
        });

        // Send deferred tracking events to analytics after a job has been processed.
        Queue::after(function () {
            Analytics::flush();
        });
    }

}
