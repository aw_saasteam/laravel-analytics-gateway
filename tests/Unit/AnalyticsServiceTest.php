<?php
namespace Aheadworks\Analytics\Gateway\Tests\Unit;

use Aheadworks\Analytics\Gateway\Service;
use Aheadworks\Analytics\Gateway\ChannelManager;
use Aheadworks\Analytics\Gateway\Channels\AbstractChannel;
use Aheadworks\Analytics\Gateway\Contracts\Data\Model\Analyzable;
use Aheadworks\Analytics\Gateway\Data\PropertiesResolver;
use Aheadworks\Analytics\Gateway\Data\Property\PropertiesCollection;
use Aheadworks\Analytics\Gateway\Exceptions\ChannelException;
use Aheadworks\Analytics\Gateway\Notifications\DiscordNotification;
use Exception;
use Illuminate\Support\Facades\Log;
use PHPUnit\Framework\MockObject\MockObject;
use Tests\TestCase;

class AnalyticsServiceTest extends TestCase
{
    /**
     * @var (ChannelManager&MockObject)|MockObject
     */
    private $channelManagerMock;

    /**
     * @var (PropertiesResolver&MockObject)|MockObject
     */
    private $propertiesResolverMock;

    private $channel;

    /**
     * @var Service
     */
    private $analyticsService;

    protected function setUp(): void
    {
        parent::setUp();
        $this->channelManagerMock = $this->createMock(ChannelManager::class);
        $this->propertiesResolverMock = $this->createMock(PropertiesResolver::class);
        $this->channel = 'debug';

        $this->analyticsService = app(Service::class, [
            'propertiesResolver' => app(PropertiesResolver::class, [
                'propertyResolvers' => [
                    'property_name_dummy'
                    /*'property_name1',
                    'property_name2'*/
                ]
            ])
        ]);
    }

    public function testIdentify(): void
    {
        $analyzableMock = $this->getMockForAbstractClass(Analyzable::class);
        $analyzableId = 1;
        $channelMock = $this->getMockForAbstractClass(AbstractChannel::class);
        $properties = ['property_name1' => 'property_value1', 'property_name2' => 'property_value2'];
        $propertyCollectionMock = $this->getMockBuilder(PropertiesCollection::class)
            ->setConstructorArgs($properties)
            ->getMock();

        try {
            $this->propertiesResolverMock
                ->expects($this->once())
                ->method('getMany')
                ->with($analyzableMock, array_keys($properties))
                ->willReturn($propertyCollectionMock);

            $this->checkRequiredAnalyzableProperties($propertyCollectionMock);

            $this->channelManagerMock
                ->expects($this->once())
                ->method('channel')
                ->with($this->channel)
                ->willReturn($channelMock);
            $analyzableMock
                ->expects($this->once())
                ->method('getAnalyzableId')
                ->willReturn($analyzableId);
            $propertyCollectionMock
                ->expects($this->once())
                ->method('toArray')
                ->willReturn($properties);
            $channelMock
                ->expects($this->once())
                ->method('identify')
                ->with($analyzableId, $properties);

            //$this->flush();
        } catch (Exception $exception) {
            Log::error($exception->getMessage());
            ChannelException::create($exception)->notify(new DiscordNotification());
        }
        $analyzableMock1 = $this->createStub(Analyzable::class);
        $this->assertNull($this->analyticsService->identify($analyzableMock1));
    }

    public function checkRequiredAnalyzableProperties($propertyCollectionMock): void
    {
        $propertyCollectionMock
            ->expects($this->once())
            ->method('keysAsArray')
            ->willReturn([]);
    }
}
