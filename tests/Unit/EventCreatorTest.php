<?php
namespace Aheadworks\Analytics\Gateway\Tests\Unit;

use Aheadworks\Analytics\Gateway\Service;
use Aheadworks\Analytics\Gateway\ChannelManager;
use Aheadworks\Analytics\Gateway\Contracts\Data\Model\Analyzable;
use Aheadworks\Analytics\Gateway\Data\AnalyzableProperties;
use Aheadworks\Analytics\Gateway\Data\PropertiesResolver;
use Aheadworks\Analytics\Gateway\Data\Property\PropertiesCollection;
use Aheadworks\Analytics\Gateway\Events\EventCreator;
use Aheadworks\Analytics\Gateway\Events\EventDefinition;
use Aheadworks\Analytics\Gateway\Events\RawEvent;
use Aheadworks\Analytics\Gateway\Exceptions\EventNotDefinedException;
use Illuminate\Support\Facades\Config;
use PHPUnit\Framework\MockObject\MockObject;
use Tests\TestCase;

class EventCreatorTest extends TestCase
{

    /**
     * @var PropertiesResolver|MockObject
     */
    private $propertiesResolverMock;

    protected function setUp(): void
    {
        parent::setUp();
        $this->channelManagerMock = $this->createMock(ChannelManager::class);
        $this->propertiesResolverMock = $this->createMock(PropertiesResolver::class);
        $this->channel = 'debug';

        $this->analyticsService = app(Service::class, [
            'propertiesResolver' => app(PropertiesResolver::class, [
                'propertyResolvers' => [
                    'property_name_dummy'
                ]
            ])
        ]);
    }

    public function testCreate()
    {
        $eventCreator = app(EventCreator::class, [
            'propertyResolver' => $this->propertiesResolverMock
        ]);

        $analyzableMock = $this->createMock(Analyzable::class);
        $eventName = 'testEvent';
        $eventClassName = RawEvent::class;

        $eventConfig = ['class_name' => $eventClassName];
        $eventData = [];
        $analyzablePropertiesMock = null;//$this->createMock(AnalyzableProperties::class);
        $hasUnresolvedProperties = false;
        Config::set("analytics.events.$eventName", $eventConfig);
        Config::set('analytics.default.event_class_name', $eventClassName);
        if (!$eventConfig) {
            $this->expectExceptionObject(new EventNotDefinedException("Event $eventName not defined."));
        }

        $eventDefinitionMock = $this->getMockBuilder(EventDefinition::class)
            ->enableOriginalConstructor()
            ->setConstructorArgs([
                'eventConfig' => $eventConfig
            ])->getMock();

        $properties = $this->getEventProperties(
            $analyzableMock,
            $eventDefinitionMock,
            $eventData
        );

        $analyzablePropertiesMock = $analyzablePropertiesMock ?? $this->createMock(AnalyzableProperties::class);

        $analyzablePropertiesMock
            ->expects($this->once())
            ->method('hasUnresolvedProperties')
            ->willReturn($hasUnresolvedProperties);
        if ($hasUnresolvedProperties) {
            //todo
        }

        $eventDefinitionMock
            ->expects($this->once())
            ->method('getClassName')
            ->willReturn($eventClassName);

        $expected = $this->getMockBuilder($eventClassName)
            ->enableOriginalConstructor()
            ->setConstructorArgs(
            [
                'analyzable' => $analyzableMock,
                'name' => $eventName,
                'properties' => $properties,
                'definition' => $eventDefinitionMock,
                'analyzableProperties' => $analyzablePropertiesMock
            ]
        )->getMock();

        $this->assertEquals($expected, $eventCreator->create(
            $analyzableMock,
            $eventName,
            $eventData,
            $analyzablePropertiesMock
        ));

    }

    private function getEventProperties(
        MockObject $analyzable,
        MockObject $eventDefinition,
        array $eventData
    ): array {
        $eventPropertyNames = [];
        $mapping = [];
        $properties = [];

        $eventDefinition
            ->expects($this->once())
            ->method('getEventPropertyNames')
            ->willReturn($eventPropertyNames);

        $propertiesToResolve = array_filter(
            $eventPropertyNames,
            function ($property) use ($eventData) {
                return !isset($eventData[$property]);
            }
        );
        $eventDefinition
            ->expects($this->at(2))
            ->method('getMapping')
            ->willReturn($mapping);

        foreach ($mapping as $search => $replacement) {
            $index = array_search($search, $propertiesToResolve);
            if ($index !== false) {
                $propertiesToResolve[$index] = $replacement;
            }
        }

        $propertyCollectionMock = $this->getMockBuilder(PropertiesCollection::class)
            ->setConstructorArgs($properties)
            ->getMock();

        $this->propertiesResolverMock
            ->expects($this->once())
            ->method('getMany')
            ->with($analyzable, $propertiesToResolve)
            ->willReturn($propertyCollectionMock);
        $propertyCollectionMock
            ->expects($this->once())
            ->method('toArray')
            ->willReturn($properties);

        $properties = array_merge(
            $properties,
            $eventData
        );

        foreach ($mapping as $replacement => $search) {
            if (isset($properties[$search])) {
                $properties[$replacement] = $properties[$search];
                unset($properties[$search]);
            }
        }

        return $properties;
    }
}
